package main

import (
	"context"
	"flag"
	"os"
	"os/signal"

	conf "gitlab.com/atsibulnik/market-service/conf"
	service "gitlab.com/atsibulnik/market-service/service"

	log "github.com/sirupsen/logrus"
)

const envPrefix = "market"

func main() {
	flag.Parse()

	config, err := conf.LoadConfig(envPrefix)
	if err != nil {
		log.WithError(err).Fatal("Error reading config")
	}

	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stderr)
	log.WithField("config", config).Info("Service configuration")

	srv := service.New(config)
	if err := srv.Open(context.Background()); err != nil {
		log.WithError(err).Fatal("Failed to start service")
	}

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt)
	<-terminate
	log.Info("service exiting")
	err = srv.Close()
	if err != nil {
		log.WithError(err).Fatal("Error stopping service")
	}
}
