package service

import (
	"context"
	"database/sql"
	"time"

	market "gitlab.com/atsibulnik/market"
	pb "gitlab.com/atsibulnik/market-service/pb"

	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

// ListAccounts returns a list of accounts
func (s *Service) ListAccounts(ctx context.Context, empty *emptypb.Empty) (*pb.AccountsList, error) {
	accounts, err := s.listAccountsImpl(ctx)
	if err != nil {
		log.WithError(err).Error("Cannot list accounts")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	return &pb.AccountsList{
		Accounts: accounts,
	}, nil
}

// CreateAccount creates new account
func (s *Service) CreateAccount(ctx context.Context, account *market.Account) (*market.Account, error) {
	if err := validateAccount(account); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	account, err := s.createAccountImpl(ctx, account)
	if err != nil {
		log.WithError(err).Error("Cannot create account")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	return account, nil
}

// GetAccount returns Account data
func (s *Service) GetAccount(ctx context.Context, req *pb.GetAccountRequest) (*market.Account, error) {
	account, err := s.getAccountByID(ctx, req.AccountId)
	if err != nil {
		log.WithField("account_id", req.AccountId).WithError(err).Error("Cannot get account")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	if account == nil {
		return nil, statusNotFound.Err()
	}
	return account, nil
}

// GetAccountPositions returns list of account positions
func (s *Service) GetAccountPositions(ctx context.Context, req *pb.GetAccountRequest) (*pb.AccountPositions, error) {
	account, err := s.getAccountByID(ctx, req.AccountId)
	if err != nil {
		log.WithField("account_id", req.AccountId).WithError(err).Error("Cannot get account")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	if account == nil {
		return nil, statusNotFound.Err()
	}

	positions, err := s.getAccountPositionsImpl(ctx, account.AccountId)
	if err != nil {
		log.WithField("account_id", req.AccountId).WithError(err).Error("Cannot list account positions")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	return &pb.AccountPositions{
		Positions: positions,
	}, nil
}

// GetAccountOrders returns list of account orders
func (s *Service) GetAccountOrders(ctx context.Context, req *pb.GetAccountRequest) (*pb.AccountOrders, error) {
	account, err := s.getAccountByID(ctx, req.AccountId)
	if err != nil {
		log.WithField("account_id", req.AccountId).WithError(err).Error("Cannot get account")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	if account == nil {
		return nil, statusNotFound.Err()
	}

	orders, err := s.getAccountOrdersImpl(ctx, account.AccountId)
	if err != nil {
		log.WithField("account_id", req.AccountId).WithError(err).Error("Cannot list account orders")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	return &pb.AccountOrders{
		Orders: orders,
	}, nil
}

func (s *Service) listAccountsImpl(ctx context.Context) ([]*market.Account, error) {
	rows, err := s.db.QueryContext(
		ctx,
		`SELECT account_id, entity_name, balance,
			created_at, updated_at
		FROM market.accounts
		WHERE deleted_at IS NULL`,
	)
	if err != nil {
		return nil, errors.Wrap(err, "query accounts")
	}
	defer rows.Close()

	accounts := []*market.Account{}
	for rows.Next() {
		var (
			id             string
			name           string
			balance        float64
			tsCreat, tsUpd *time.Time
		)
		if err := rows.Scan(&id, &name, &balance, &tsCreat, &tsUpd); err != nil {
			return nil, errors.Wrap(err, "scan account")
		}
		account := &market.Account{
			Metadata:   &market.Meta{},
			AccountId:  id,
			EntityName: name,
			Balance:    balance,
		}
		if tsCreat != nil {
			account.Metadata.CreatedAt, _ = ptypes.TimestampProto(*tsCreat)
		}
		if tsUpd != nil {
			account.Metadata.UpdatedAt, _ = ptypes.TimestampProto(*tsUpd)
		}

		accounts = append(accounts, account)
	}

	return accounts, nil
}

// CreateAccount creates new account
func (s *Service) createAccountImpl(ctx context.Context, account *market.Account) (*market.Account, error) {
	row := s.db.QueryRowContext(
		ctx,
		`INSERT INTO market.accounts (entity_name, balance) VALUES ($1, $2) RETURNING account_id`,
		account.EntityName, account.Balance,
	)

	if err := row.Scan(&account.AccountId); err != nil {
		return nil, errors.Wrap(err, "create account")
	}
	return account, nil
}

func (s *Service) getAccountByID(ctx context.Context, id string) (*market.Account, error) {
	var tsCreat, tsUpd *time.Time
	account := &market.Account{AccountId: id, Metadata: &market.Meta{}}
	row := s.db.QueryRowContext(
		ctx,
		`SELECT entity_name, balance, created_at, updated_at from market.accounts
		WHERE account_id = $1 AND deleted_at IS NULL`,
		id,
	)

	if err := row.Scan(
		&account.EntityName, &account.Balance,
		&tsCreat, &tsUpd,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, errors.Wrap(err, "query account")
	}

	if tsCreat != nil {
		account.Metadata.CreatedAt, _ = ptypes.TimestampProto(*tsCreat)
	}
	if tsUpd != nil {
		account.Metadata.UpdatedAt, _ = ptypes.TimestampProto(*tsUpd)
	}

	return account, nil
}

func (s *Service) getAccountPositionsImpl(ctx context.Context, id string) ([]*market.Position, error) {
	rows, err := s.db.QueryContext(
		ctx,
		`SELECT position_id, order_id, asset_id, size, price, position
		FROM market.positions
		WHERE account_id = $1 AND deleted_at IS NULL`, id,
	)
	if err != nil {
		return nil, errors.Wrap(err, "query account positions")
	}
	defer rows.Close()

	positions := []*market.Position{}
	for rows.Next() {
		pos := &market.Position{}

		if err := rows.Scan(
			&pos.PositionId, &pos.OrderId, &pos.AssetId,
			&pos.Size, &pos.Price, &pos.PositionId,
		); err != nil {
			return nil, errors.Wrap(err, "scan position")
		}

		pos.AccountId = id
		positions = append(positions, pos)
	}

	return positions, nil
}

func (s *Service) getAccountOrdersImpl(ctx context.Context, id string) ([]*market.Order, error) {
	rows, err := s.db.QueryContext(
		ctx,
		`SELECT order_id, asset_id, type, size, price, 
			matched_at, created_at, updated_at
		FROM market.orders
		WHERE account_id = $1 AND deleted_at IS NULL`, id,
	)
	if err != nil {
		return nil, errors.Wrap(err, "query account orders")
	}
	defer rows.Close()

	orders := []*market.Order{}
	for rows.Next() {
		var (
			tsMatch, tsCreat, tsUpd *time.Time
			orderType               string
			order                   = &market.Order{Metadata: &market.Meta{}}
		)

		if err := rows.Scan(
			&order.OrderId, &order.AssetId, &orderType,
			&order.Size, &order.Price, &tsMatch, &tsCreat, &tsUpd,
		); err != nil {
			return nil, errors.Wrap(err, "scan order")
		}

		if tsMatch != nil {
			order.MatchedAt, _ = ptypes.TimestampProto(*tsMatch)
		}
		if tsCreat != nil {
			order.Metadata.CreatedAt, _ = ptypes.TimestampProto(*tsCreat)
		}
		if tsUpd != nil {
			order.Metadata.UpdatedAt, _ = ptypes.TimestampProto(*tsUpd)
		}

		order.Type = market.OrderType(market.OrderType_value[orderType])
		order.AccountId = id
		orders = append(orders, order)
	}

	return orders, nil
}

func validateAccount(account *market.Account) error {
	if account == nil {
		return errors.New("Empty account")
	}
	if account.EntityName == "" {
		return errors.New("Missing entity_name parameter")
	}
	if account.Balance < 0 {
		return errors.New("Balance cannot be negative")
	}
	return nil
}
