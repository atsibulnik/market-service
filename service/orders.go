package service

import (
	"context"
	"database/sql"
	"time"

	market "gitlab.com/atsibulnik/market"

	"github.com/golang/protobuf/ptypes/timestamp"

	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CreateOrder creates new order
func (s *Service) CreateOrder(ctx context.Context, order *market.Order) (*market.Order, error) {
	if err := validateOrder(order); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Check if corresponding account exists
	account, err := s.getAccountByID(ctx, order.AccountId)
	if err != nil {
		log.WithField("account_id", order.AccountId).WithError(err).Error("Cannot get account for order")
		return nil, status.Error(codes.FailedPrecondition, "get account for order")
	}
	if account == nil {
		return nil, status.Error(codes.FailedPrecondition, "account not found")
	}
	// Check that account balance will not go below 0
	if order.Type == market.OrderType_BUY && account.Balance < order.Price {
		return nil, status.Error(codes.FailedPrecondition, "low account balance")
	}

	// For sell order we must create corresponding asset
	// or return error if asset already exists
	// For buy orders corresponding asset must exist
	asset, err := s.getAssetByID(ctx, order.AssetId)
	if err != nil {
		log.WithField("asset_id", order.AssetId).WithError(err).Error("Cannot get asset for order")
		return nil, status.Error(codes.FailedPrecondition, "get asset for order")
	}
	if order.Type == market.OrderType_BUY {
		if asset == nil {
			return nil, status.Error(codes.FailedPrecondition, "asset not found")
		}
	} else {
		if asset != nil {
			return nil, status.Error(codes.FailedPrecondition, "asset already exists")
		}
	}

	order, err = s.createOrderImpl(ctx, order)
	if err != nil {
		log.WithError(err).Error("Cannot create order")
		if s.config.Debug {
			return nil, statusWithDetails(statusInternalError, err).Err()
		}
		return nil, statusInternalError.Err()
	}
	return order, nil
}

// CreateOrder creates new order
func (s *Service) createOrderImpl(ctx context.Context, order *market.Order) (*market.Order, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, errors.Wrap(err, "begin transaction")
	}

	// Create asset if sell order is placed
	if order.Type == market.OrderType_SELL {
		row := tx.QueryRowContext(
			ctx,
			`INSERT INTO market.assets (account_id, size, price) 
			VALUES ($1, $2, $3) RETURNING asset_id`,
			order.AccountId, order.Size, order.Price,
		)
		if err := row.Scan(&order.AssetId); err != nil {
			return nil, rollbackTx(tx, err, "insert asset for order")
		}
	}

	// Create order itself
	row := tx.QueryRowContext(
		ctx,
		`INSERT INTO market.orders (account_id, asset_id, type, size, price) 
		VALUES ($1, $2, $3, $4, $5) RETURNING order_id`,
		order.AccountId, order.AssetId, order.Type.String(), order.Size, order.Price,
	)
	if err := row.Scan(&order.OrderId); err != nil {
		return nil, rollbackTx(tx, err, "insert order")
	}

	if order.Type == market.OrderType_BUY {
		// Reduce investor's balance
		if _, err := tx.ExecContext(
			ctx,
			`UPDATE market.accounts SET balance = balance - $1
		WHERE account_id = $2`,
			order.Price, order.AccountId,
		); err != nil {
			return nil, rollbackTx(tx, err, "update balance")
		}

		// Do matching
		matchedAt, err := s.bidMatcher(ctx, tx, order.AssetId)
		if err != nil {
			return nil, rollbackTx(tx, err, "bid matcher")
		}
		order.MatchedAt = matchedAt
	}

	if err := tx.Commit(); err != nil {
		return nil, errors.Wrap(err, "commit order")
	}
	return order, nil
}

func (s *Service) bidMatcher(ctx context.Context, tx *sql.Tx, assetID string) (*timestamp.Timestamp, error) {
	var (
		sellOrder               *market.Order
		buyOrders               = []*market.Order{}
		targetSize, targetPrice float64
		totalSize, totalPrice   float64
		hasMatch                bool
		lastIndexMatch          int
		finalPrice              float64
		tsMatch                 *time.Time
		matchedAt               *timestamp.Timestamp
	)

	rows, err := tx.QueryContext(ctx,
		`SELECT 
			order_id, account_id, size, price, type, matched_at,
			1 - price/size as discount
		FROM market.orders
		WHERE asset_id = $1 AND deleted_at IS NULL
		ORDER BY discount, created_at
		FOR UPDATE`,
		assetID,
	)
	if err != nil {
		return nil, errors.Wrap(err, "query orders for bid")
	}
	defer rows.Close()
	for rows.Next() {
		var (
			discount  float64
			orderType string
			order     = &market.Order{}
		)
		if err := rows.Scan(
			&order.OrderId, &order.AccountId, &order.Size,
			&order.Price, &orderType, &tsMatch, &discount,
		); err != nil {
			return nil, errors.Wrap(err, "scan orders for asset")
		}
		if tsMatch != nil {
			order.MatchedAt, _ = ptypes.TimestampProto(*tsMatch)
		}

		order.Type = market.OrderType(market.OrderType_value[orderType])

		if order.Type == market.OrderType_SELL {
			sellOrder = order
			targetPrice = sellOrder.Price
			targetSize = sellOrder.Size
		} else {
			buyOrders = append(buyOrders, order)
		}
	}

	if sellOrder == nil {
		return nil, errors.New("No corresponding sell order found")
	}

	for i, order := range buyOrders {
		totalSize += order.Size
		totalPrice += order.Price

		// If all the bids placed fill 100% of the invoice size and the price offered is greater or
		// equal to the amount asked by the seller the algorithm has a match and the invoice is
		// considered financed
		if totalSize >= targetSize && totalPrice >= targetPrice {
			// Must trim last bid size and price and re-check if we still have a match
			if totalSize > targetSize {
				quot := (order.Size - (totalSize - targetSize)) / order.Size
				trimPrice := totalPrice - order.Price + order.Price*quot
				if quot <= 0 {
					// Cannot much with current order configuration
					break
				}
				if trimPrice >= targetPrice {
					hasMatch = true
					lastIndexMatch = i
					finalPrice = trimPrice
					order.Size = order.Size - (totalSize - targetSize)
					order.Price = order.Price * quot
					break
				}
			} else { // totalSize == targetSize
				hasMatch = true
				lastIndexMatch = i
				finalPrice = totalPrice
				break
			}
		}
	}

	if hasMatch {
		log.WithFields(log.Fields{
			"sell_order_id":    sellOrder.OrderId,
			"sell_order_size":  targetSize,
			"sell_order_price": targetPrice,
			"asset_id":         sellOrder.AssetId,
			"buy_orders_cnt":   lastIndexMatch + 1,
			"final_price":      finalPrice,
		}).Info("Sell order matched")

		// Finalize invoice financing
		for i := 0; i <= lastIndexMatch; i++ {
			order := buyOrders[i]
			// Update order's matched_at
			if _, err := tx.ExecContext(
				ctx,
				`UPDATE market.orders SET matched_at = now() at time zone 'UTC'
			WHERE order_id = $1`, order.OrderId,
			); err != nil {
				return nil, errors.Wrapf(err, "update order matched_at for %s", order.OrderId)
			}
			// Insert position
			if _, err := tx.ExecContext(
				ctx,
				`INSERT INTO market.positions (order_id, account_id, asset_id, size, price, position) 
				VALUES ($1, $2, $3, $4, $5, $6)`,
				order.OrderId, order.AccountId, assetID, order.Size, order.Price, i,
			); err != nil {
				return nil, errors.Wrapf(err, "update order matched_at for %s", order.OrderId)
			}
		}
		// Update Sell order
		row := tx.QueryRowContext(
			ctx,
			`UPDATE market.orders SET matched_at = now() at time zone 'UTC'
		WHERE order_id = $1 RETURNING matched_at`, sellOrder.OrderId,
		)
		if err := row.Scan(&tsMatch); err != nil {
			return nil, errors.Wrapf(err, "scan matched_at for %s", sellOrder.OrderId)
		}
		if tsMatch != nil {
			matchedAt, _ = ptypes.TimestampProto(*tsMatch)
		}
		// Update seller's balance
		if _, err := tx.ExecContext(
			ctx,
			`UPDATE market.accounts SET balance = balance + $1 WHERE account_id = $2`,
			finalPrice, sellOrder.AccountId,
		); err != nil {
			return nil, errors.Wrapf(err, "update account balance for %s", sellOrder.AccountId)
		}
	}

	return matchedAt, nil
}

func validateOrder(order *market.Order) error {
	if order == nil {
		return errors.New("Empty order")
	}
	if order.AccountId == "" {
		return errors.New("Missing account_id parameter")
	}
	if order.Type == market.OrderType_BUY && order.AssetId == "" {
		return errors.New("Missing asset_id parameter")
	}
	if order.Price <= 0.0 || order.Size <= 0.0 {
		return errors.New("Order price and size must be above 0")
	}
	return nil
}

func rollbackTx(tx *sql.Tx, err error, msg string) error {
	if err2 := tx.Rollback(); err2 != nil {
		return errors.Wrapf(err2, "rollback %s", msg)
	}
	return errors.Wrap(err, msg)
}
