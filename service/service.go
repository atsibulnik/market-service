package service

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"time"

	conf "gitlab.com/atsibulnik/market-service/conf"

	pb "gitlab.com/atsibulnik/market-service/pb"

	"google.golang.org/protobuf/types/known/emptypb"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"

	"golang.org/x/sync/errgroup"

	gwrt "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

const sqlDriver = "postgres"

var statusInternalError = status.New(codes.Internal, "internal error")
var statusNotFound = status.New(codes.NotFound, "not found")

// Service is main GRPC service
type Service struct {
	config   *conf.Config
	db       *sql.DB
	grpc     *grpc.Server
	gw       *gwrt.ServeMux
	grpcAddr string
	gwAddr   string
	listen   net.Listener
	server   *http.Server
}

// New instantiates new service
func New(config *conf.Config) *Service {
	s := Service{
		config:   config,
		grpcAddr: config.GRPC.Host + ":" + fmt.Sprintf("%d", config.GRPC.Port),
		gwAddr:   config.HTTP.Host + ":" + fmt.Sprintf("%d", config.HTTP.Port),
		grpc:     grpc.NewServer(),
	}

	pb.RegisterMarketServer(s.grpc, &s)

	// Init GRPC gateway
	if s.config.HTTP.Enabled {
		s.gw = gwrt.NewServeMux(
			gwrt.WithProtoErrorHandler(customHTTPError),
			gwrt.WithMarshalerOption(gwrt.MIMEWildcard, &gwrt.JSONPb{OrigName: true, EmitDefaults: true}),
		)
	}

	return &s
}

// Open starts the service
func (s *Service) Open(ctx context.Context) error {

	db, err := openDB(sqlDriver, s.config.DB.DSN())
	if err != nil {
		if err, ok := err.(*pq.Error); ok {
			return errors.Wrap(err, "pq error")
		}
		return errors.Wrap(err, "connect db")
	}
	s.db = db

	ln, err := net.Listen("tcp", s.grpcAddr)
	if err != nil {
		return errors.Wrap(err, "listen grpc")
	}
	s.listen = ln

	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		log.WithField("grpc_address", s.listen.Addr().String()).Info("Started GRPC server")
		return errors.Wrap(s.grpc.Serve(s.listen), "serve grpc")
	})

	// Start gateway if configured
	if s.config.HTTP.Enabled {
		g.Go(func() error {
			ctx := context.Background()
			ctx, cancel := context.WithCancel(ctx)
			defer cancel()

			err := pb.RegisterMarketHandlerFromEndpoint(
				ctx,
				s.gw,
				s.grpcAddr,
				[]grpc.DialOption{grpc.WithInsecure()},
			)
			if err != nil {
				return err
			}

			log.WithField("gateway_address", s.gwAddr).Info("Started GRPC Gatewaty")
			return http.ListenAndServe(
				s.gwAddr,
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					s.gw.ServeHTTP(w, r)
				}))

		})
	}

	return g.Wait()
}

// Close stops the service
func (s *Service) Close() error {
	log.Info("service stopped")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := s.server.Shutdown(ctx)
	if err != nil {
		return err
	}
	return nil
}

// CheckHealth returns service health
func (s *Service) CheckHealth(ctx context.Context, empty *emptypb.Empty) (*pb.HealthCheckResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	err := s.db.PingContext(ctx)
	if err != nil {
		log.WithError(err).Error("db ping failed")
		return nil, statusInternalError.Err()
	}
	return &pb.HealthCheckResponse{
		Status:  "pass",
		Version: "1",
	}, nil
}

func openDB(driver, dsn string) (*sql.DB, error) {
	log.WithField("dsn", dsn).Info("Connecting to database")
	db, err := sql.Open(driver, dsn)
	if err != nil {
		log.WithError(err).Error("Cannot connect to database.")
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		log.WithError(err).Error("Cannot ping database.")
		return nil, err
	}
	log.Info("Successfully connected to database.")
	return db, nil
}

type errorBody struct {
	Err     string        `json:"error,omitempty"`
	Details []interface{} `json:"details,omitempty"`
}

func customHTTPError(ctx context.Context, _ *gwrt.ServeMux, marshaler gwrt.Marshaler, w http.ResponseWriter, _ *http.Request, err error) {
	w.Header().Set("Content-type", marshaler.ContentType())
	w.WriteHeader(gwrt.HTTPStatusFromCode(grpc.Code(err)))

	jErr := json.NewEncoder(w).Encode(errorBody{
		Err:     status.Convert(err).Message(),
		Details: status.Convert(err).Details(),
	})

	if jErr != nil {
		w.Write([]byte(`{"error": "failed to marshal error message"}`))
	}
}

func statusWithDetails(st *status.Status, err error) *status.Status {
	extStatus, e := st.WithDetails(&pb.DebugInfo{
		InternalError: err.Error(),
	})
	if e != nil {
		return st
	}
	return extStatus
}
