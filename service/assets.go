package service

import (
	"context"
	"database/sql"
	"time"

	market "gitlab.com/atsibulnik/market"

	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

func (s *Service) createAssetImpl(ctx context.Context, asset *market.Asset, autoCommit bool) (*market.Asset, error) {
	rows, err := s.db.QueryContext(
		ctx,
		`INSERT INTO market.assets (account_id, size, price) VALUES ($1, $2, $3, $4, $5)`,
		asset.AccountId, asset.Size, asset.Price,
	)
	if err != nil {
		return nil, errors.Wrap(err, "create asset")
	}
	rows.Close()
	return asset, nil
}

func (s *Service) getAssetByID(ctx context.Context, id string) (*market.Asset, error) {
	var tsCreat, tsUpd *time.Time
	asset := &market.Asset{AssetId: id, Metadata: &market.Meta{}}
	row := s.db.QueryRowContext(
		ctx,
		`SELECT account_id, size, price,
			created_at, updated_at from market.assets
		WHERE asset_id = $1 AND deleted_at IS NULL`,
		id,
	)

	if err := row.Scan(&asset.AccountId, &asset.Size, &asset.Price, &tsCreat, &tsUpd); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, errors.Wrap(err, "query asset")
	}

	if tsCreat != nil {
		asset.Metadata.CreatedAt, _ = ptypes.TimestampProto(*tsCreat)
	}
	if tsUpd != nil {
		asset.Metadata.UpdatedAt, _ = ptypes.TimestampProto(*tsUpd)
	}

	return asset, nil
}
