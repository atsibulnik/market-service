package conf

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

// Config is a structure holding configuration being read from env
type Config struct {
	DB    DBConfig   `json:"db"`
	HTTP  HTTPConfig `json:"http"`
	GRPC  GRPCConfig `json:"grpc"`
	Debug bool       `envconfig:"debug" default:"false" json:"debug"`
}

// DBConfig holds database configuration attributes
type DBConfig struct {
	Host     string `envconfig:"host" default:"localhost" json:"host"`
	Port     int    `envconfig:"port" default:"5432" json:"port"`
	User     string `envconfig:"dbuser" default:"market" json:"user"`
	Password string `envconfig:"password" default:"market" json:"password"`
	Database string `envconfig:"database" default:"market" json:"database"`
	Debug    bool   `envconfig:"debug" default:"false" json:"debug"`
}

// HTTPConfig defines HTTP configuration options
type HTTPConfig struct {
	Host    string `envconfig:"host" default:"0.0.0.0" json:"host"`
	Port    int    `envconfig:"port" default:"8080" json:"port"`
	Enabled bool   `envconfig:"enabled" default:"false" json:"enabled"`
}

// GRPCConfig defines GRPC configuration options
type GRPCConfig struct {
	Host string `envconfig:"host" default:"0.0.0.0" json:"host"`
	Port int    `envconfig:"port" default:"9000" json:"port"`
}

// DSN produces DSN string
func (c *DBConfig) DSN() string {
	return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%d sslmode=disable binary_parameters=yes",
		c.User, c.Password, c.Database, c.Host, c.Port)
}

// LoadConfig load configuration from environment
func LoadConfig(prefix string) (*Config, error) {
	var res Config
	err := errors.Wrap(envconfig.Process(prefix, &res), "loading configuration")
	return &res, err
}
