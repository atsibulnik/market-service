CREATE SEQUENCE market.assets_id_seq;

CREATE TABLE market.assets
(
	asset_id	varchar(12) NOT NULL DEFAULT 'AR'||to_char(nextval('market.assets_id_seq'), 'FM0000000000'),
	account_id	varchar(12) NOT NULL,
	size            numeric NOT NULL,
	price		numeric NOT NULL,
	created_at	timestamp without time zone DEFAULT (now() at time zone 'UTC'),
	updated_at	timestamp without time zone,
	deleted_at	timestamp without time zone
);

ALTER TABLE market.assets
	 ADD CONSTRAINT pk_assets_asset_id PRIMARY KEY (asset_id);
