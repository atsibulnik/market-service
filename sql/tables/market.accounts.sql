CREATE SEQUENCE market.accounts_id_seq;

CREATE TABLE market.accounts
(
	account_id	varchar(12) NOT NULL DEFAULT 'EN'||to_char(nextval('market.accounts_id_seq'), 'FM0000000000'),
	entity_name	text NOT NULL,
	balance		numeric NOT NULL CHECK (balance >= 0),
	created_at	timestamp without time zone DEFAULT (now() at time zone 'UTC'),
	updated_at	timestamp without time zone,
	deleted_at	timestamp without time zone
);

ALTER TABLE market.accounts
	 ADD CONSTRAINT pk_accounts_account_id PRIMARY KEY (account_id);
