CREATE TYPE market.order_type AS ENUM ('SELL', 'BUY');

CREATE SEQUENCE market.orders_id_seq;

CREATE TABLE market.orders
(
	order_id	varchar(12) NOT NULL DEFAULT 'OR'||to_char(nextval('market.orders_id_seq'), 'FM0000000000'),
	account_id	varchar(12) NOT NULL,
	asset_id	varchar(12) NOT NULL,
	type		market.order_type NOT NULL,
	size		numeric NOT NULL,
	price		numeric NOT NULL,
	matched_at	timestamp without time zone,
	created_at	timestamp without time zone DEFAULT (now() at time zone 'UTC'),
	updated_at	timestamp without time zone,
	deleted_at	timestamp without time zone
);

ALTER TABLE market.orders
	ADD CONSTRAINT pk_orders_order_id PRIMARY KEY (order_id);
