SELECT 
    o.order_id, o.type, o.size, o.price, o.matched_at,
    1 - o.price/o.size as discount,
    a.account_id, a.balance as account_balance
FROM market.orders o 
    JOIN market.accounts a ON o.account_id = a.account_id
WHERE o.asset_id = 'AR0000000007' AND o.deleted_at IS NULL 
         AND a.deleted_at IS NULL AND o.type='BUY'
ORDER BY discount, o.created_at;
