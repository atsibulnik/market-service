CREATE TABLE market.positions
(
	position_id	SERIAL,
	order_id	varchar(12) NOT NULL,
	account_id	varchar(12) NOT NULL,
	asset_id	varchar(12) NOT NULL,
	size		numeric NOT NULL,
	price		numeric NOT NULL,
	position	integer,
	created_at	timestamp without time zone DEFAULT (now() at time zone 'UTC'),
	updated_at	timestamp without time zone,
	deleted_at	timestamp without time zone
);

ALTER TABLE market.positions
	ADD CONSTRAINT pk_positions_position_id PRIMARY KEY (position_id);
